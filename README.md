[![](https://img.shields.io/discord/669249617539498023.svg?logo=Discord&logoColor=white&label=discord)](https://discord.gg/hmWDMH8)

# LAWF - Lol, Another Web Framework

A from-scratch web framework written in python allowing one to create simple web servers for hosting RESTful apis or web pages.

This project is currently in pre-alpha. Breaking changes may be introduced at any time with no warning. The project is currently only suitable for providing an extremely simple interface.

[Repository](https://gitlab.com/tandemdude/lawf)

[Documentation](https://tandemdude.gitlab.io/lawf)

## Installing

```
pip install lawf
```

## Creating a simple web application

```python
import lawf

server = lawf.Server(port=8080)


@server.uri_rule("/hello")
def hello_world(request):
    return "Hello World!"


server.run()
```
