.. LAWF documentation master file, created by
   sphinx-quickstart on Sat Jan 25 12:50:27 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: https://img.shields.io/discord/669249617539498023.svg?logo=Discord&logoColor=white&label=discord
    :target: https://discord.gg/hmWDMH8

.. image:: https://badgen.net/pypi/v/lawf
    :target: https://pypi.org/project/lawf/

.. image:: https://badgen.net/pypi/license/lawf
.. image:: https://img.shields.io/pypi/status/lawf
.. image:: https://img.shields.io/pypi/pyversions/lawf
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg

----

Lol, Another Web Framework
==========================

A from-scratch web framework written in python allowing one to create simple web servers for hosting RESTful apis or web pages.

This project is currently in pre-alpha. Breaking changes may be introduced at any time with no warning. The project is currently only suitable for providing a simple interface for the serving of files and fulfilling of requests.

If you need any support you can join the `discord server <https://discord.gg/hmWDMH8>`_

.. toctree::
    :maxdepth: 2

    quickstart
    api-reference

* :ref:`genindex`
