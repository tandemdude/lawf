==========
Quickstart
==========


Installing
==========

LAWF can be installed directly from PyPi using pip.

OSX/Linux:
``$ python3 -m pip install lawf --user``

Windows:
``$ py -m pip install lawf --user``

.. note:: 
    LAWF is currently only compatible with **python 3.8** as it has not yet been tested with other versions.


Creating A Simple Server
========================

Your own web-server can be written in just a few lines of code:
::

    # Import the library
    import lawf

    # Create an instance of the Server class
    server = lawf.Server()

    # Define a URI rule for the server
    @server.uri_rule("/hello")
    def hello_world(request):
        # Return "Hello World!" when a request is made to the URI /hello
        return "Hello World!"

    # Run the server
    server.run()

When you run this code, the server should start up and should print a message to console that
looks something like this:
``Server listening on 127.0.0.1:8080``

Now if you open a browser and go to the URL ``127.0.0.1:8080/hello``, the message "Hello World!" should be displayed
on the webpage.