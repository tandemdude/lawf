import setuptools


def long_description():
    with open("README.md") as fp:
        return fp.read()


setuptools.setup(
    name="lawf",
    packages=["lawf"],
    version="0.0.8",
    license="MIT",
    description="A simple, from-scratch web framework for development of RESTful APIs and serving web pages.",
    long_description=long_description(),
    long_description_content_type="text/markdown",
    author="tandemdude",
    author_email="tandemdude1@gmail.com",
    url="https://gitlab.com/tandemdude/lawf",
    install_requires=[],
    python_requires=">=3.8.0",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: 3 :: Only",
    ],
)
