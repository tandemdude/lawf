import nox

CODE_DIR = "lawf"


@nox.session(reuse_venv=True)
def format(session):
	session.run("python3", "-m", "pip", "install", "-U", "black")
	session.run("black", CODE_DIR, "--check")


@nox.session(reuse_venv=True)
def gendoc(session):
    session.run("python3", "-m", "pip", "install", "-U", "sphinx")
    session.run("python3", "-m", "pip", "install", "-U", "sphinx_rtd_theme")
    session.run("python3", "-m" , "sphinx.cmd.build", "docs/source", "docs/build", "-b", "html")
