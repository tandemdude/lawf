import os


class File:
    """
    Base class representing a file object. Reads the bytes of the file into an attribute
    and provides the :meth:`.File.get_headers` which returns the appropriate headers for the filetype.
    
    Args:
        filepath (str): The path to the file to be read.

    Attributes:
        filepath (str): The path to the file.
        data (bytes): The bytes read from the file. 

    .. note::
        This base class should not be instantiated by a user.

    """

    def __init__(self, filepath):
        self.filepath = filepath
        self.data = self.read()

    def __repr__(self):
        return f"<{self.__class__.__name__}>"

    def read(self):
        """
        Reads and returns the :class:`bytes` from the file.

        Returns:
            :class:`bytes` read from the file.
        """
        with open(self.filepath, "rb") as fp:
            return fp.read()

    def get_headers(self):
        """
        Returns the appropriate HTTP headers for any supported file type.
        These headers include ``Content-Type`` and ``Content-Length``.

        Returns:
            :class:`dict` containing the appropriate headers.

        """
        pass


class PlaintextFile(File):
    def get_headers(self):
        """"""
        return {"Content-Type": "text/plain", "Content-Length": len(self.data)}


class RichtextFile(File):
    def get_headers(self):
        """"""
        return {"Content-Type": "application/rtf", "Content-Length": len(self.data)}


class HTMLFile(File):
    def get_headers(self):
        """"""
        return {"Content-Type": "text/html", "Content-Length": len(self.data)}


class CSSFile(File):
    def get_headers(self):
        """"""
        return {"Content-Type": "text/css", "Content-Length": len(self.data)}


class JSFile(File):
    def get_headers(self):
        """"""
        return {"Content-Type": "text/javascript", "Content-Length": len(self.data)}


class JPEGFile(File):
    def get_headers(self):
        """"""
        return {"Content-Type": "image/jpeg", "Content-Length": len(self.data)}


class GIFFile(File):
    def get_headers(self):
        """"""
        return {"Content-Type": "image/gif", "Content-Length": len(self.data)}


class PNGFile(File):
    def get_headers(self):
        """"""
        return {"Content-Type": "image/png", "Content-Length": len(self.data)}


class ICOFile(File):
    def get_headers(self):
        """"""
        return {
            "Content-Type": "image/vnd.microsoft.icon",
            "Content-Length": len(self.data),
        }


class CSVFile(File):
    def get_headers(self):
        """"""
        return {"Content-Type": "text/csv", "Content-Length": len(self.data)}


class PDFFile(File):
    def get_headers(self):
        """"""
        return {"Content-Type": "application/pdf", "Content-Length": len(self.data)}


class PHPFile(File):
    def get_headers(self):
        """"""
        return {"Content-Type": "application/php", "Content-Length": len(self.data)}


class BinaryFile(File):
    def get_headers(self):
        """"""
        return {
            "Content-Type": "application/octet-stream",
            "Content-Length": len(self.data),
        }


class BitmapFile(File):
    def get_headers(self):
        """"""
        return {"Content-Type": "image/bmp", "Content-Length": len(self.data)}


class JSONFile(File):
    def get_headers(self):
        """"""
        return {"Content-Type": "application/json", "Content-Length": len(self.data)}


class JSONLDFile(File):
    def get_headers(self):
        """"""
        return {"Content-Type": "application/ld+json", "Content-Length": len(self.data)}


class SVGFile(File):
    def get_headers(self):
        """"""
        return {"Content-Type": "image/svg+xml", "Content-Length": len(self.data)}


class WEBPFile(File):
    def get_headers(self):
        """"""
        return {"Content-Type": "image/webp", "Content-Length": len(self.data)}


class TIFFile(File):
    def get_headers(self):
        """"""
        return {"Content-Type": "image/tiff", "Content-Length": len(self.data)}
