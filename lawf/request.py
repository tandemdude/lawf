import socket
import datetime
import json


class Request:
    """
    A class representing the request recieved from the client.

    Args:
        connection (socket.socket): The socket to recieve the request message from.
        address (tuple): The IP and port that the client connected with.

    Attributes:
        connection (socket.socket): The socket the message was recieved through.
        head (str): The head of the message.
        body (str): The body of the message.
        headline (str): The first line of the message.
        headers (dict): The headers present in the request.
        method (str): The HTTP method used by the request.
        uri (str): The request URI, also known as the route.
        proto (str): The protocol used by the request.

    """

    def __init__(self, connection, address):
        time_of_request = datetime.datetime.utcnow()

        self.connection = connection

        self.head = None
        self.body = None

        self.headline = None
        self.headers = None

        self.method = None
        self.uri = None
        self.proto = None

        payload = self.receive_data()
        self.decode_payload(payload)
        print(
            f"{self.method} {self.uri} from {address[0]} @ {time_of_request.strftime('%d/%m %X')}"
        )

    def __repr__(self):
        return f"<Request [{self.method}][{self.uri}]>"

    @property
    def json(self):
        """
        Converts the recieved message's body from json to dict.

        Returns:
            :class:`dict` message body decoded from json.
        """
        return json.loads(self.body)

    def receive_data(self):
        """
        Recieves the request data over the socket connection.

        Returns:
            :class:`str` recived data
        """
        received = self.connection.recv(4096)
        return received.decode("utf-8")

    def decode_payload(self, payload):
        """
        Reads the data recieved over the socket connection into the classes attributes.

        Args:
            payload (str): The payload to read the data from

        Returns:
            ``None``
        """
        data = payload
        request = "".join((line + "\n") for line in data.splitlines())
        self.head, self.body = request.split("\n\n", 1)
        self.head = self.head.splitlines()
        self.headline = self.head[0]
        self.headers = dict(x.split(": ", 1) for x in self.head[1:])
        self.method, self.uri, self.proto = self.headline.split(" ", 3)
        self.uri = self.uri.lower()
