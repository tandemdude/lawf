import os

from . import filetypes
from .response import Response

FILE_MAP = {
    "txt": filetypes.PlaintextFile,
    "rtf": filetypes.RichtextFile,
    "html": filetypes.HTMLFile,
    "htm": filetypes.HTMLFile,
    "css": filetypes.CSSFile,
    "js": filetypes.JSFile,
    "mjs": filetypes.JSFile,
    "jpeg": filetypes.JPEGFile,
    "jpg": filetypes.JPEGFile,
    "gif": filetypes.GIFFile,
    "png": filetypes.PNGFile,
    "ico": filetypes.ICOFile,
    "csv": filetypes.CSVFile,
    "pdf": filetypes.PDFFile,
    "php": filetypes.PHPFile,
    "bin": filetypes.BinaryFile,
    "bmp": filetypes.BitmapFile,
    "json": filetypes.JSONFile,
    "jsonld": filetypes.JSONLDFile,
    "svg": filetypes.SVGFile,
    "webp": filetypes.WEBPFile,
    "tif": filetypes.TIFFile,
    "tiff": filetypes.TIFFile,
}


def _check_if_html_dir_exists():
    if not os.path.isdir("public_html"):
        raise errors.HTMLDirectoryNotFoundError(
            "A directory called public_html could not be found. Either it does not exist or is not in the root directory of the project."
        )


def serve_file(route_to_file, *, standard_location=True):
    """
    Creates a valid :class:`lawf.response.Response` from a given file path including
    the correct headers.

    Args:
        route_to_file (str): The path to the file to create a response from.
        standard_location (bool, optional): Whether the file is in the ``public_html/`` directory or not.
            If True, ``"public_html"`` will be prepended to the file path before attempting to create the response.

    Returns:
        :class:`lawf.response.Response` created based from the file type.

    Example:
    
        .. code-block:: python

            server = lawf.Server()

            @server.uri_rule("/test")
            def test(request):
                return lawf.serve_file("test.html")

    """
    extension = route_to_file.split(".")[-1].strip().strip("\n")

    if extension not in FILE_MAP.keys():
        print(
            f"[WARN] Request made for {route_to_file} could not be fulfilled as the filetype is not supported."
        )
        return Response(500)

    try:
        if standard_location:
            _check_if_html_dir_exists()
            filetype = FILE_MAP[extension]
            file = filetype(os.path.join("public_html", route_to_file))
        else:
            file = FILE_MAP[extension](route_to_file)
    except FileNotFoundError:
        print(
            f"[WARN] Request made for {route_to_file} could not be fulfilled as the file could not be found"
        )
        return Response(404)

    response = Response(200, body_bytes=file.data)
    headers = file.get_headers()
    for key, value in headers.items():
        response.headers[key] = value

    return response
