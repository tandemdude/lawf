from .server import Server

myserver = Server(port=8080)


@myserver.uri_rule("/hello")
def hello(request):
    return "Hello World!"


myserver.run()
