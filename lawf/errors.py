class HTMLDirectoryNotFoundError(OSError):
    """
    Thrown when a file was requested from the default path but no directory
    named public_html could be found.

    Args:
        message (str): Human readable string describing the exception.
    """

    def __init__(self, message):
        self.message = message
