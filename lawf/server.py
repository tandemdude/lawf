from concurrent import futures
import socket
import json
import os

from .request import Request
from .response import Response
from . import serving

DEFAULT_BACKLOG = 2
DEFAULT_MAX_THREADS = 5
REQUEST_METHODS = (
    "OPTIONS",
    "GET",
    "HEAD",
    "POST",
    "PUT",
    "DELETE",
)


def _validate_args(route, methods):
    if not isinstance(route, str):
        raise ValueError("Route must be a string starting with /")
    elif not route.startswith("/"):
        raise ValueError("Route must be a string starting with /")
    if not isinstance(methods, list):
        raise ValueError("Methods must be a list")


def _create_response_from_call(route, request):
    route_output = route(request)

    if isinstance(route_output, Response):
        response = route_output
    elif isinstance(route_output, str):
        response = Response(200, route_output)
        response.headers["Content-Type"] = (
            "text/html" if "<html>" in route_output else "text/plain"
        )
    elif isinstance(route_output, int):
        response = Response(route_output)
    elif isinstance(route_output, dict):
        response = Response(200, json.dumps(route_output))
        response.headers["Content-Type"] = "application/json"
    elif not route_output:
        response = Response(204)
    else:
        response = Response(*route_output)

    return response


class URIRule:
    """
    A class representing a single rule stored in :attr:`.Server.routes`.

    Args:
        function (callable): The function or callable a route is bound to.
        allowed_methods (list of str): The HTTP methods allowed for the callable.
    
    """

    def __init__(self, function, allowed_methods):
        self.function = function
        self.allowed_methods = allowed_methods

    def __call__(self, request):
        if request.method in self.allowed_methods:
            return self.function(request)


class Server:
    """
    A class representing the webserver. Handles socket binding and passes any requests off into
    the thread pool to be handled by a worker.

    All arguments are keyword-only.

    Args:
        host (str, optional): The IP to bind the socket to. Defaults to 127.0.0.1
        port (int, optional): The port to bind the socket to. Defaults to 8080
        backlog (int, optional): The number of unaccepted connections that the system will allow before refusing new connections. Defaults to 2.
        max_threads (int, optional): The maximum number of workers in the thread pool. Defaults to 5.
        auto_serve_html (bool, optional): Whether or not the server should automatically serve HTML files when requested.
            If False, the server will not automatically serve HTML files when they are requested. 
            You must serve them manually. Defaults to False.
    
    Attributes:
        routes (dict): A mapping of all valid registered request routes to the function to call when a request to that URI is recieved.
        host (str): The IP to bind the socket to. Defaults to 127.0.0.1
        port (int): The port to bind the socket to. Defaults to 8080
        listen_backlog (int): The number of unaccepted connections that the system will allow before refusing new connections. Defaults to 2.
        auto_serve_html (bool): Whether or not the server should automatically serve HTML files when requested.
        thread_pool (concurrent.futures.ThreadPoolExecutor): The thread pool to pass requests into for processing.

    """

    routes = dict()

    def __init__(
        self,
        *,
        host="127.0.0.1",
        port=8080,
        backlog=DEFAULT_BACKLOG,
        max_threads=DEFAULT_MAX_THREADS,
        auto_serve_html=False,
    ):
        self.host = host
        self.port = port
        self.listen_backlog = backlog
        self.auto_serve_html = auto_serve_html

        self.thread_pool = futures.ThreadPoolExecutor(max_workers=max_threads)

    def run(self):
        """
        Creates and binds a :class:`socket.socket` to the host and port specified during initialisation.
        Calls :meth:`socket.socket.listen` and then accepts any new connections and passes them off to the
        thread pool to be fulfilled.

        Returns:
            ``None``

        """
        print("Starting server")
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as serversocket:
            serversocket.bind((self.host, self.port))
            serversocket.listen(self.listen_backlog)
            print(f"Server listening on {self.host}:{self.port}")
            while True:
                connection, address = serversocket.accept()
                self.thread_pool.submit(self.submit_request, connection, address)

    @staticmethod
    def uri_rule(route, *, methods=["GET"]):
        """
        An alternative to using :meth:`.Server.add_uri_rule` for the adding of URI rules.
        This is used as a decorator to simplify the process.
        Note that all functions to bind must take a single argument which will be an instance of :class:`lawf.request.Request`

        Args:
            route (str): The route to bind a function to, for example ``"/hello"``.
            methods (list of str, optional): The list of methods allowed for this route. Defaults to ``["GET"]``.
        
        Example:

            .. code-block:: python

                server = lawf.Server()

                @server.uri_rule("/foo")
                def foo(request):
                    return "bar"

        See Also:
            :meth:`.Server.add_uri_rule` for valid function return types.

        """
        _validate_args(route, methods)

        def decorate(func):
            if not Server.routes.get(func):
                Server.routes[route.lower()] = URIRule(func, methods)

        return decorate

    def add_uri_rule(self, route, function, *, methods=["GET"]):
        """
        Links a function to a URI and stores the relationship in the :attr:`.Server.routes` dict
        for later lookup.
        Note that all callables to bind must take a single argument which will be an instance of :class:`lawf.request.Request`

        Args:
            route (str): The route to bind a function to, for example ``"/hello"``.
            function (callable): The function or other callable to bind to the route.
            methods (list of str, optional): The list of methods allowed for this route. Defaults to ``["GET"]``.
        
        Returns:
            ``None``

        Example:
        
            .. code-block:: python

                server = lawf.Server()

                def foo(request):
                    return "bar"

                server.add_uri_rule("/foo", foo)

        See Also:
            :meth:`.Server.uri_rule`
        

        Callable Return Types:
            Many different return types are supprted from the function/callable passed into this method.
            These are outlined below:

            Integer HTTP status code:
                For example ``return 200``.
                This will create a response of the status code relating to the return value, with an empty response body.

            String:
                For example ``return "Hello World!".
                This will create a response of status ``200`` with the body containing the returned string. The server will
                attempt to guess whether the response is raw HTML or just plaintext. This means that returning raw HTML is
                supported as well, for example ``return "<html><body>Hello World!</body></html>"``.

            Dictionary:
                For example ``return {"status": "success"}``.
                This will create a response of status ``200`` with the header ``Content-Type: application/json`` and the body
                containing the json-parsed dict.

            Response instance:
                For example ``return Response(200)``.
                This will skip creating a response as you provided one already. Read the docs on :class:`lawf.response.Response`
                for details on the initialisation parameters.

            Multiple Values:
                For example ``return 200, "Hello World!"``.
                The order of the return values is **important**, it must be ``int, string`` representing the status code, and
                the response body.

        """
        _validate_args(route, methods)
        Server.routes[route.lower()] = URIRule(function, methods)

    def submit_request(self, connection, address):
        """
        Takes a new socket connection and creates a :class:`lawf.request.Request` instance containing
        the data from the request submitted by the connection. Attempts to match the request URI to one
        registered by a user. If a valid URI is not found then the server attempts to match it to a
        file in the ``public_html/`` directory.
        Finally, sends the apropriate response through the socket connection and closes it.

        Args:
            connection (socket.socket): The socket to communicate to the client through.
            address (tuple): The IP and port the client connected using.

        Returns:
            ``None``

        """
        request = Request(connection, address)
        route = Server.routes.get(request.uri, None)

        if route is None:
            if request.uri.endswith(".html"):
                if self.auto_serve_html:
                    response = serving.serve_file(request.uri.strip("/"))
            else:
                response = serving.serve_file(request.uri.strip("/"))

        elif request.method == "OPTIONS":
            response = Response(200)
            response.headers["Allow"] = ", ".join(route.allowed_methods)

        elif request.method == "HEAD" and "GET" in route.allowed_methods:
            response = _create_response_from_call(route, request)
            response.body = ""
            response.body_bytes = None

        elif request.method not in route.allowed_methods:
            response = Response(405)
            response.headers["Allow"] = ", ".join(route.allowed_methods)

        else:
            response = _create_response_from_call(route, request)

        response.connection = connection

        response.send()
        connection.close()
