import lawf

server = lawf.Server(port=8080)


@server.uri_rule("/hello", methods=["GET"])
def hello_world(request):
    return "Hello World!"


server.run()
